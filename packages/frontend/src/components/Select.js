import React from 'react';

export default function Select(props) {

    const { value, onChange, tooltip, tooltipLength, children } = props

    return (
        <div
            className="flex-row items-start"
            data-balloon-pos="up"
            data-balloon-blunt
            aria-label={tooltip}
            data-balloon-length={tooltipLength}>


            <select
                onChange={onChange}
                className="bg-transparent h-full py-1 border border-gray-300 hover:ring-4 rounded focus:outline-none focus:ring-2"
                value={value}
            >
                {children}
            </select>
        </div>
    )
}