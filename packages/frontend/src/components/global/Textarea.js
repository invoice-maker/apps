import React from 'react';

export default function Textarea(props) {

    const { selected, customClass, onChange, placeholder, rows, required, name } = props

    return (
        <textarea
            name={name}
            onChange={onChange}
            className={`p-1 border border-gray-300 w-full hover:ring-4 focus:outline-none focus:ring-2 ${customClass || ''}`}
            value={selected}
            placeholder={placeholder}
            rows={rows}
            required={required}
        >
        </textarea>
    )
}