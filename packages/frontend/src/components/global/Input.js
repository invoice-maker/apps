import React from 'react';

export default function Input(props) {

    const { id, inputType, inputClass, placeholder, parentClass, onChange, selected, name } = props

    return (
        <div className={`w-full flex items-center border border-gray-300 w-full  bg-white
            ${parentClass}
        `}>
            <input
                name={name}
                style={{ fontFamily: 'DejaVu Sans' }}
                id={id}
                className={`w-full min-w-0 block p-1 hover:ring-4 focus:outline-none focus:ring-2 ${inputClass}`}
                type={inputType}
                placeholder={placeholder}
                onChange={onChange}
                value={selected}
            />
        </div>
    )
}