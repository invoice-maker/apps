import React from 'react';

export default function Checkbox(props) {

    const { id, checked, onChange, label } = props

    return (
        <label
            htmlFor={id}
            className="flex flex-row space-x-2 items-center cursor-pointer hover:ring-4 focus:outline-none focus:ring-2">

            <input
                className="cursor-pointer"
                type="checkbox"
                id={id}
                checked={checked}
                onChange={onChange}
            />

            <div>
                {label}
            </div>
        </label>
    )
}