import React from 'react';

export default function MenuItem(props) {

    const { onChange } = props
    return (
        <option onClick={onChange} value="">(None)</option>
    )
}
