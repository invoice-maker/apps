import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { setLabel } from '../model/workspace/labels.slice';

export default function LabelInput(props) {

    const { labelName, customClass } = props
    const dispatch = useDispatch()
    const label = useSelector((state) => state.invoiceLabelsConfigs.labels[labelName]);

    return (
        <input
            className={`p-1 w-full rounded bg-transparent hover:ring-4 focus:outline-none focus:ring-2 ${customClass || ''}`}
            value={label}
            onChange={(e) => dispatch(setLabel({ labelName, value: e.target.value }))}
        />
    )
}