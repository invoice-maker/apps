import React from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default function Datepicker(props) {

    const { inputClass, parentClass, onChange, unix_date, string_date } = props

    return (
        <div tabIndex="-1" className={`w-full flex items-center border border-gray-300 w-full rounded bg-white hover:ring-4 focus:outline-none focus:ring-2
        ${parentClass}
        `}>

            <DatePicker
                className={`w-full min-w-0 block p-1 text-right ${inputClass} focus:outline-none focus:ring-2 rounded ${inputClass} `}
                selected={unix_date}
                onChange={onChange}
                value={string_date}
            />

        </div>
    );
}