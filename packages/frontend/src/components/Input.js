import React from 'react';
import { useSelector } from 'react-redux';

export default function Input(props) {

    const currency_symbol = useSelector(state => state.invoiceItems.currency_symbol)
    const currency_symbol_on_left = useSelector(state => state.invoiceItems.currency_symbol_on_left)
    const custom_currency_simbol = useSelector(state => state.invoiceItems.custom_currency_simbol)

    const usedCurrencySymbol = custom_currency_simbol || currency_symbol

    const { id, isCurrency, isPercent, inputType, inputClass, placeholder, parentClass, onChange, selected } = props

    const currencySign = isCurrency ? <div className="w-auto px-1">{usedCurrencySymbol}</div> : null;
    const percentSign = isPercent ? <div className="w-auto px-1">%</div> : null;

    const parentOutlineClass = isCurrency || isPercent ? null : 'hover:ring-4 focus:outline-none focus:ring-2';
    const inputOutlineClass = isCurrency || isPercent ? 'hover:ring-4 focus:outline-none focus:ring-2' : null;

    return (
        <div className={`w-full flex items-center border border-gray-300 w-full rounded  bg-white
            ${parentOutlineClass}
            ${isCurrency ? 'flex-row' : 'flex-row-reverse ' + parentClass}
            ${currency_symbol_on_left ? '' : 'flex-row-reverse'}
        `}>
            {currencySign}
            {percentSign}
            <input
                style={{fontFamily: 'DejaVu Sans'}}
                id={id}
                className={`w-full min-w-0 block p-1 ${inputOutlineClass} focus:outline-none focus:ring-2 rounded ${inputClass}`}
                type={inputType}
                placeholder={placeholder}
                onChange={onChange}
                value={selected}
            />
        </div>
    )
}