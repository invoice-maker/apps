import React from 'react';

export default function Textarea(props) {

    const { selected, customClass, onChange } = props

    return (
        <textarea
            onChange={onChange}
            className={`p-1 border border-gray-300 rounded w-full hover:ring-4 focus:outline-none focus:ring-2 ${customClass || ''}`}
            value={selected}
        >
        </textarea>
    )
}