import React from 'react';

export default function Button(props) {

    const { color, text, size, tooltip, tooltipSize, onClick, width, fontSize, fontColor } = props;

    const buttonColor = {
        white: 'from-gray-300 to-gray-400',
        yellow: 'from-yellow-300 to-yellow-400',
        green: 'from-green-500 to-green-700',
        pink: 'from-pink-500 to-pink-700',
        blue: 'from-blue-500 to-blue-700',
        indigo: 'from-indigo-500 to-indigo-700',
        gray: 'from-gray-500 to-gray-700',
        red: 'from-red-300 to-red-400',
        purple: 'from-purple-500 to-purple-700'
    }

    const borderColor = {
        white: 'border-gray-400',
        red: 'border-red-600',
        blue: 'border-blue-600',
        pink: 'border-pink-600',
        yellow: 'border-yellow-600',
        indigo: 'border-indigo-600',
        purple: 'border-purple-600',
        gray: 'border-gray-600',
        green: 'border-green-600'
    }

    const buttonSize = {
        small: 'p-1 text-sm',
        medium: 'py-1 px-2',
        big: 'py-2 px-4',
        large: 'p-4'
    }

    const buttonWidth = {
        full: 'w-full'
    }

    const textSize = {
        base: 'font-base',
        xl: 'text-xl',
        xl2: 'text-2xl',
        xl3: 'text-3xl'
    }

    const textColor = {
        black: 'text-black',
        white: 'text-white'
    }

    return (
        <button className={`
             ${buttonSize[size] || buttonSize['medium']} 

            ${buttonWidth[width]}
            ${textSize[fontSize] || textSize['base']}
            ${textColor[fontColor] || textColor['black']}
            border bg-gradient-to-b hover:bg-gradient-to-t 
            ${buttonColor[color]}  hover:shadow-none border 
            ${borderColor[color]} rounded-lg hover:bg-yellow-700 hover:ring-4 focus:ring-4
            shadow hover:shadow-none
            `}
            data-balloon-pos="up"
            data-balloon-blunt
            aria-label={tooltip}
            data-balloon-length={tooltipSize || "large"}
            onClick={onClick}
        >
            {text}
        </button>
    )
}