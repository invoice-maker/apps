import React from 'react';
import { useSelector } from 'react-redux';

export default function LabelCurrency(props) {
    const { valueClass, currencyClass, value } = props

    const locale = useSelector(state => state.invoiceItems.locale)
    const currency_symbol = useSelector(state => state.invoiceItems.currency_symbol)
    const currencyOnLeft = useSelector((state) => state.invoiceItems.currency_symbol_on_left)

    const balanceDue = new Intl.NumberFormat(locale).format(value)

    return (
        <div className={`
            flex flex-row justify-between ${currencyClass}
            ${currencyOnLeft ? '' : 'flex-row-reverse'}
        `}
        >
            <div>
                {currency_symbol}
            </div>
            <div className={`w-full overflow-x-auto ${currencyOnLeft ? 'ml-1' : 'mr-1'} text-right ${valueClass}`}>
                {balanceDue}
            </div>
        </div>
    )
}