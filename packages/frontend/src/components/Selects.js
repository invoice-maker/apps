import React from 'react';

export default function Select(props) {

    const { options, value, onChange, customClass } = props

    const optionsElement = options.map(item => {
        if (item.type === 'group') {
            return (<optgroup key={item.label} label={item.label}>
                {item.items.map(childItem => {
                    return (
                        <option key={childItem.value} value={childItem.value}>
                            {childItem.label}
                        </option>
                    )
                })}

            </optgroup>)
        } else {
            return (
                <option key={item.value} value={item.value}>
                    {item.name}
                </option>
            )
        }
    })


    return (
        <select
            className={`p-2 bg-white border w-full hover:ring-4 focus:outline-none focus:ring-2
            ${customClass || ""}
            `}
            value={value}
            onChange={onChange}
        >
            {optionsElement}
        </select>
    )
}