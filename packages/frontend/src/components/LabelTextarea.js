import React, { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { setLabel } from '../model/workspace/labels.slice';

export default function LabelTextarea(props) {

    const textarea = useRef(null);
    const { rows, customClass, labelName } = props

    const dispatch = useDispatch()
    const label = useSelector((state) => state.invoiceLabelsConfigs.labels[labelName]);

    // resize textarea label after every render
    useEffect(() => {

        const currentTextarea = textarea.current

        currentTextarea.setAttribute('style', 'height:' + (currentTextarea.scrollHeight) + 'px;overflow-y:hidden;');
        currentTextarea.style.height = 'auto';
        currentTextarea.style.height = (currentTextarea.scrollHeight) + 'px';

    }, [label])

    return (
        <textarea
            ref={textarea}
            className={`p-1 w-full rounded hover:ring-4 focus:outline-none focus:ring-2 ${customClass || ''}`}
            rows={rows || "1"}
            value={label}
            onChange={(e) => dispatch(setLabel({ labelName, value: e.target.value }))}
        >

        </textarea>
    )
}