let configs = {
    GENERATOR_API_BASE_URL: process.env.NODE_ENV === 'development' ? 'http://localhost:5005/v1' : 'https://vivainvoice-backend-api.web.app/v1'
}


export default configs