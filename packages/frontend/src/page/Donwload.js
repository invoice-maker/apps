import React from 'react';
import { useSelector } from 'react-redux';
import Input from '../components/global/Input';
import Textarea from '../components/global/Textarea';

export default function Download() {

    const generateInvoiceLoadingState = useSelector(state => state.loadingState)

    let content
    if (generateInvoiceLoadingState.loadingGenerateInvoicePDF === 'pending') {
        content = (
            <>
                <h1 className=" text-2xl">
                    Please wait your invoice is being generated
                </h1>

                <h2 className="text-lg my-8">
                    Your download should start automatically in a few seconds
                </h2>
            </>
        )
    }

    if (generateInvoiceLoadingState.loadingGenerateInvoicePDF === 'rejected') {
        content = (
            <>
                <h1 className=" text-2xl">
                    Failed to generate your invoice :(
                </h1>

                <h2 className="text-lg my-8">
                    Sorry something is wrong, failed to generate your invoice. Please try again later.
                </h2>
            </>
        )
    }

    if (generateInvoiceLoadingState.loadingGenerateInvoicePDF === 'fulfilled') {
        content = (
            <>
                <h1 className=" text-2xl">
                    Your invoice is ready :)
                </h1>

                <h2 className="text-lg my-8">
                    We hope your experience was awesome and we can’t wait to see you again soon.
                </h2>
            </>
        )
    }

    let announcement = (
        <div className="flex flex-col items-center justify-center w-full text-gray-700">
            <div className="flex flex-col p-8 bg-white shadow my-8 md:w-1/2">
                <h1 className="mt-2 mb-8 text-2xl">
                    We are still baby 👶
            </h1>

                <h2 className="text-lg leading-relaxed space-y-4">
                    <p>
                        Hi there thank you for invoicing with us.
                        As we are just getting started, our service is still in under heavy development.
                </p>

                    <div className="p-2 bg-yellow-100 border border-yellow-300 rounded">
                        Currently we have not implemented feature to retain your invoice datas.
                        Your invoice datas will be erased after you refresh or close your browser.
                </div>

                    <p>
                        Though our recent release have some limited features, we have these features to be implemented on our radar:
                </p>

                    <ul className="list-disc ml-8">
                        <li>Use image logo</li>
                        <li><strike>Make UI responsive</strike></li>
                        <li>Store your invoice datas on your machine</li>
                        <li>RTL support</li>
                    </ul>

                    <p>
                        We have a lot of features idea to be implemented,
                        but those 3 features are on our priority list.
                </p>
                    <p>
                        So we are very sorry for the inconvenience, <br />
                    We hope you had a good experience with us and come back again later :)
                </p>

                </h2>

            </div>
        </div>
    )


    let feedback = (
        <div className="flex flex-col items-center justify-center w-full text-gray-700">
            <div className="flex flex-col p-8 bg-white shadow my-8 md:w-1/2">
                <h1 className="mt-2 mb-8 text-2xl">
                    Help us grow ❤️
                    </h1>

                <div className="text-lg leading-relaxed space-y-4">
                    <p>
                        Help us to understand more about your problem.
                        What do you need and expect from invoice generator app like us?
                        </p>
                    <form className="space-y-4" method="POST" action="https://api.formcubes.com/submission/3MxrKyFonDOy5hnXeATS">
                        <Input name="email" placeholder="Email (optional)" />
                        <Textarea name="message" placeholder="Tell us what do you think" rows="4" required={true} />
                        <div className="g-recaptcha" data-sitekey="6Lfvj8EUAAAAAOWJGlV090uDY5-xs21ZZJc0F3Ie"></div>
                        <button type="submit" className="bg-pink-600 text-white py-2 px-6 hover:ring-4 focus:outline-none focus:ring-2 hover:bg-pink-500" >Send</button>
                    </form>
                </div>

            </div>
        </div>
    )




    return (
        <div className="flex flex-col w-full">
            <div className="flex flex-col items-center justify-center w-full text-gray-700">
                <div className="flex flex-col p-8 bg-white shadow my-8 rounded items-center">
                    {content}

                </div>
            </div>

            {generateInvoiceLoadingState.loadingGenerateInvoicePDF === 'fulfilled' ? announcement : null}

            {generateInvoiceLoadingState.loadingGenerateInvoicePDF === 'fulfilled' ? feedback : null}

        </div>
    )
}