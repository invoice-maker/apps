import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { saveAs } from 'file-saver';
import axios from 'axios';
import configs from '../configs/configs';

const generateInvoicePDF = createAsyncThunk(
    'loading/generateinvoicePDF',
    async (payload, thunk) => {
        const response = await axios.post(configs.GENERATOR_API_BASE_URL, payload, {
            responseType: 'arraybuffer',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/pdf'
            }
        })

        const blob = new Blob([response.data], { type: "application/pdf;charset=utf-8" });

        saveAs(blob, payload.header.invoice_number);

        return true
    }
)

export const loadingSlice = createSlice({
    name: 'loading',
    initialState: {
        loadingGenerateInvoicePDF: undefined
    },
    reducers: {},
    extraReducers: {
        [generateInvoicePDF.fulfilled]: (state, action) => {
            state.loadingGenerateInvoicePDF = 'fulfilled'
        },
        [generateInvoicePDF.pending]: (state, action) => {
            state.loadingGenerateInvoicePDF = 'pending'
        },
        [generateInvoicePDF.rejected]: (state, action) => {
            state.loadingGenerateInvoicePDF = 'rejected'
        }
    }
})

export { generateInvoicePDF }

export default loadingSlice.reducer