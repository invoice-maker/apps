import { createSlice } from '@reduxjs/toolkit'
import dayjs from 'dayjs';


const calculateDueDate = (state, value, name) => {

    if (value !== "" && value !== 'custom') {

        const invoice_date = state.invoice_date.unix_date
        const span = parseInt(value.slice(0, -1))
        const period = value.slice(-1)

        state[name].string_date = dayjs(invoice_date).add(span, period).format(state.date_format)
        state[name].unix_date = dayjs(invoice_date).add(span, period).valueOf()
    } else if(value === 'custom'){
        return state
    } else {
        state[name].string_date = ""
        state[name].unix_date = null
    }

    return state

}

export const metaSlice = createSlice({
    name: 'meta',
    initialState: {
        date_format: 'DD MMM YYYY',
        invoice_id: null,
        invoice_date: {
            string_date: dayjs().format('DD MMM YYYY'),
            unix_date: dayjs().valueOf() // milisecond unix date
        },
        invoice_due_date: {
            due_date_span: "",
            string_date: null,
            unix_date: null // milisecond unix date
        },
    },
    reducers: {
        reFormatDate: (state, action) => {
            const date_format = state.date_format
            const invoice_date = state.invoice_date.unix_date
            const invoice_due_date = state.invoice_due_date.unix_date

            state.invoice_date.string_date = dayjs(invoice_date).format(date_format)

            if (invoice_due_date) {
                state.invoice_due_date.string_date = dayjs(invoice_due_date).format(date_format)
            }

            return state
        },
        setStringValue: (state, action) => {
            const { name, value } = action.payload
            state[name] = value

        },
        setDateValue: (state, action) => {
            const { name, value } = action.payload

            state[name].string_date = dayjs(value).format(state.date_format)
            state[name].unix_date = dayjs(value).valueOf()

        },
        setDueDateSpan: (state, action) => {
            const { name, value } = action.payload
            state[name].due_date_span = value

            state = calculateDueDate(state, value, name)

        },
        reCalculateDueDate: (state, action) => {

            const { name } = action.payload
            const value = state.invoice_due_date.due_date_span

            state = calculateDueDate(state, value, name)

        }
    }
})

export const { reFormatDate, setDueDateSpan, setDateValue, setStringValue, reCalculateDueDate } = metaSlice.actions


export default metaSlice.reducer