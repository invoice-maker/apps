import { createSlice } from '@reduxjs/toolkit'

export const exportOptionsSlice = createSlice({
    name: 'export-options',
    initialState: { 
        paper_size: 'A4'
     },
    reducers: {
        setPaperSize: (state, action) => {
            const { value } = action.payload
            state.paper_size = value

            return state
        }
    }
})


export const { setPaperSize } = exportOptionsSlice.actions


export default exportOptionsSlice.reducer