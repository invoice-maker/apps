const currency = [
    {
        currency_code: "AED",
        currency_name: "UAE Dirham"
    },
    {
        currency_code: "AFN",
        currency_name: "Afghani"
    },
    {
        currency_code: "ALL",
        currency_name: "Lek"
    },
    {
        currency_code: "AMD",
        currency_name: "Armenian Dram"
    },
    {
        currency_code: "ANG",
        currency_name: "Netherlands Antillean Guilder"
    },
    {
        currency_code: "AOA",
        currency_name: "Kwanza"
    },
    {
        currency_code: "ARS",
        currency_name: "Argentine Peso"
    },
    {
        currency_code: "AUD",
        currency_name: "Australian Dollar"
    },
    {
        currency_code: "AWG",
        currency_name: "Aruban Florin"
    },
    {
        currency_code: "AZN",
        currency_name: "Azerbaijan Manat"
    },
    {
        currency_code: "BAM",
        currency_name: "Convertible Mark"
    },
    {
        currency_code: "BBD",
        currency_name: "Barbados Dollar"
    },
    {
        currency_code: "BDT",
        currency_name: "Taka"
    },
    {
        currency_code: "BGN",
        currency_name: "Bulgarian Lev"
    },
    {
        currency_code: "BHD",
        currency_name: "Bahraini Dinar"
    },
    {
        currency_code: "BIF",
        currency_name: "Burundi Franc"
    },
    {
        currency_code: "BMD",
        currency_name: "Bermudian Dollar"
    },
    {
        currency_code: "BND",
        currency_name: "Brunei Dollar"
    },
    {
        currency_code: "BOB",
        currency_name: "Boliviano"
    },
    {
        currency_code: "BOV",
        currency_name: "Mvdol"
    },
    {
        currency_code: "BRL",
        currency_name: "Brazilian Real"
    },
    {
        currency_code: "BSD",
        currency_name: "Bahamian Dollar"
    },
    {
        currency_code: "BTN",
        currency_name: "Ngultrum"
    },
    {
        currency_code: "BWP",
        currency_name: "Pula"
    },
    {
        currency_code: "BYN",
        currency_name: "Belarusian Ruble"
    },
    {
        currency_code: "BZD",
        currency_name: "Belize Dollar"
    },
    {
        currency_code: "CAD",
        currency_name: "Canadian Dollar"
    },
    {
        currency_code: "CDF",
        currency_name: "Congolese Franc"
    },
    {
        currency_code: "CHE",
        currency_name: "WIR Euro"
    },
    {
        currency_code: "CHF",
        currency_name: "Swiss Franc"
    },
    {
        currency_code: "CHW",
        currency_name: "WIR Franc"
    },
    {
        currency_code: "CLF",
        currency_name: "Unidad de Fomento"
    },
    {
        currency_code: "CLP",
        currency_name: "Chilean Peso"
    },
    {
        currency_code: "CNY",
        currency_name: "Yuan Renminbi"
    },
    {
        currency_code: "COP",
        currency_name: "Colombian Peso"
    },
    {
        currency_code: "COU",
        currency_name: "Unidad de Valor Real"
    },
    {
        currency_code: "CRC",
        currency_name: "Costa Rican Colon"
    },
    {
        currency_code: "CUC",
        currency_name: "Peso Convertible"
    },
    {
        currency_code: "CUP",
        currency_name: "Cuban Peso"
    },
    {
        currency_code: "CVE",
        currency_name: "Cabo Verde Escudo"
    },
    {
        currency_code: "CZK",
        currency_name: "Czech Koruna"
    },
    {
        currency_code: "DJF",
        currency_name: "Djibouti Franc"
    },
    {
        currency_code: "DKK",
        currency_name: "Danish Krone"
    },
    {
        currency_code: "DOP",
        currency_name: "Dominican Peso"
    },
    {
        currency_code: "DZD",
        currency_name: "Algerian Dinar"
    },
    {
        currency_code: "EGP",
        currency_name: "Egyptian Pound"
    },
    {
        currency_code: "ERN",
        currency_name: "Nakfa"
    },
    {
        currency_code: "ETB",
        currency_name: "Ethiopian Birr"
    },
    {
        currency_code: "EUR",
        currency_name: "Euro"
    },
    {
        currency_code: "FJD",
        currency_name: "Fiji Dollar"
    },
    {
        currency_code: "FKP",
        currency_name: "Falkland Islands Pound"
    },
    {
        currency_code: "GBP",
        currency_name: "Pound Sterling"
    },
    {
        currency_code: "GEL",
        currency_name: "Lari"
    },
    {
        currency_code: "GHS",
        currency_name: "Ghana Cedi"
    },
    {
        currency_code: "GIP",
        currency_name: "Gibraltar Pound"
    },
    {
        currency_code: "GMD",
        currency_name: "Dalasi"
    },
    {
        currency_code: "GNF",
        currency_name: "Guinean Franc"
    },
    {
        currency_code: "GTQ",
        currency_name: "Quetzal"
    },
    {
        currency_code: "GYD",
        currency_name: "Guyana Dollar"
    },
    {
        currency_code: "HKD",
        currency_name: "Hong Kong Dollar"
    },
    {
        currency_code: "HNL",
        currency_name: "Lempira"
    },
    {
        currency_code: "HRK",
        currency_name: "Kuna"
    },
    {
        currency_code: "HTG",
        currency_name: "Gourde"
    },
    {
        currency_code: "HUF",
        currency_name: "Forint"
    },
    {
        currency_code: "IDR",
        currency_name: "Rupiah"
    },
    {
        currency_code: "ILS",
        currency_name: "New Israeli Sheqel"
    },
    {
        currency_code: "INR",
        currency_name: "Indian Rupee"
    },
    {
        currency_code: "IQD",
        currency_name: "Iraqi Dinar"
    },
    {
        currency_code: "IRR",
        currency_name: "Iranian Rial"
    },
    {
        currency_code: "ISK",
        currency_name: "Iceland Krona"
    },
    {
        currency_code: "JMD",
        currency_name: "Jamaican Dollar"
    },
    {
        currency_code: "JOD",
        currency_name: "Jordanian Dinar"
    },
    {
        currency_code: "JPY",
        currency_name: "Yen"
    },
    {
        currency_code: "KES",
        currency_name: "Kenyan Shilling"
    },
    {
        currency_code: "KGS",
        currency_name: "Som"
    },
    {
        currency_code: "KHR",
        currency_name: "Riel"
    },
    {
        currency_code: "KMF",
        currency_name: "Comorian Franc "
    },
    {
        currency_code: "KPW",
        currency_name: "North Korean Won"
    },
    {
        currency_code: "KRW",
        currency_name: "Won"
    },
    {
        currency_code: "KWD",
        currency_name: "Kuwaiti Dinar"
    },
    {
        currency_code: "KYD",
        currency_name: "Cayman Islands Dollar"
    },
    {
        currency_code: "KZT",
        currency_name: "Tenge"
    },
    {
        currency_code: "LAK",
        currency_name: "Lao Kip"
    },
    {
        currency_code: "LBP",
        currency_name: "Lebanese Pound"
    },
    {
        currency_code: "LKR",
        currency_name: "Sri Lanka Rupee"
    },
    {
        currency_code: "LRD",
        currency_name: "Liberian Dollar"
    },
    {
        currency_code: "LSL",
        currency_name: "Loti"
    },
    {
        currency_code: "LYD",
        currency_name: "Libyan Dinar"
    },
    {
        currency_code: "MAD",
        currency_name: "Moroccan Dirham"
    },
    {
        currency_code: "MDL",
        currency_name: "Moldovan Leu"
    },
    {
        currency_code: "MGA",
        currency_name: "Malagasy Ariary"
    },
    {
        currency_code: "MKD",
        currency_name: "Denar"
    },
    {
        currency_code: "MMK",
        currency_name: "Kyat"
    },
    {
        currency_code: "MNT",
        currency_name: "Tugrik"
    },
    {
        currency_code: "MOP",
        currency_name: "Pataca"
    },
    {
        currency_code: "MRU",
        currency_name: "Ouguiya"
    },
    {
        currency_code: "MUR",
        currency_name: "Mauritius Rupee"
    },
    {
        currency_code: "MVR",
        currency_name: "Rufiyaa"
    },
    {
        currency_code: "MWK",
        currency_name: "Malawi Kwacha"
    },
    {
        currency_code: "MXN",
        currency_name: "Mexican Peso"
    },
    {
        currency_code: "MXV",
        currency_name: "Mexican Unidad de Inversion (UDI)"
    },
    {
        currency_code: "MYR",
        currency_name: "Malaysian Ringgit"
    },
    {
        currency_code: "MZN",
        currency_name: "Mozambique Metical"
    },
    {
        currency_code: "NAD",
        currency_name: "Namibia Dollar"
    },
    {
        currency_code: "NGN",
        currency_name: "Naira"
    },
    {
        currency_code: "NIO",
        currency_name: "Cordoba Oro"
    },
    {
        currency_code: "NOK",
        currency_name: "Norwegian Krone"
    },
    {
        currency_code: "NPR",
        currency_name: "Nepalese Rupee"
    },
    {
        currency_code: "NZD",
        currency_name: "New Zealand Dollar"
    },
    {
        currency_code: "OMR",
        currency_name: "Rial Omani"
    },
    {
        currency_code: "PAB",
        currency_name: "Balboa"
    },
    {
        currency_code: "PEN",
        currency_name: "Sol"
    },
    {
        currency_code: "PGK",
        currency_name: "Kina"
    },
    {
        currency_code: "PHP",
        currency_name: "Philippine Peso"
    },
    {
        currency_code: "PKR",
        currency_name: "Pakistan Rupee"
    },
    {
        currency_code: "PLN",
        currency_name: "Zloty"
    },
    {
        currency_code: "PYG",
        currency_name: "Guarani"
    },
    {
        currency_code: "QAR",
        currency_name: "Qatari Rial"
    },
    {
        currency_code: "RON",
        currency_name: "Romanian Leu"
    },
    {
        currency_code: "RSD",
        currency_name: "Serbian Dinar"
    },
    {
        currency_code: "RUB",
        currency_name: "Russian Ruble"
    },
    {
        currency_code: "RWF",
        currency_name: "Rwanda Franc"
    },
    {
        currency_code: "SAR",
        currency_name: "Saudi Riyal"
    },
    {
        currency_code: "SBD",
        currency_name: "Solomon Islands Dollar"
    },
    {
        currency_code: "SCR",
        currency_name: "Seychelles Rupee"
    },
    {
        currency_code: "SDG",
        currency_name: "Sudanese Pound"
    },
    {
        currency_code: "SEK",
        currency_name: "Swedish Krona"
    },
    {
        currency_code: "SGD",
        currency_name: "Singapore Dollar"
    },
    {
        currency_code: "SHP",
        currency_name: "Saint Helena Pound"
    },
    {
        currency_code: "SLL",
        currency_name: "Leone"
    },
    {
        currency_code: "SOS",
        currency_name: "Somali Shilling"
    },
    {
        currency_code: "SRD",
        currency_name: "Surinam Dollar"
    },
    {
        currency_code: "SSP",
        currency_name: "South Sudanese Pound"
    },
    {
        currency_code: "STN",
        currency_name: "Dobra"
    },
    {
        currency_code: "SVC",
        currency_name: "El Salvador Colon"
    },
    {
        currency_code: "SYP",
        currency_name: "Syrian Pound"
    },
    {
        currency_code: "SZL",
        currency_name: "Lilangeni"
    },
    {
        currency_code: "THB",
        currency_name: "Baht"
    },
    {
        currency_code: "TJS",
        currency_name: "Somoni"
    },
    {
        currency_code: "TMT",
        currency_name: "Turkmenistan New Manat"
    },
    {
        currency_code: "TND",
        currency_name: "Tunisian Dinar"
    },
    {
        currency_code: "TOP",
        currency_name: "Pa’anga"
    },
    {
        currency_code: "TRY",
        currency_name: "Turkish Lira"
    },
    {
        currency_code: "TTD",
        currency_name: "Trinidad and Tobago Dollar"
    },
    {
        currency_code: "TWD",
        currency_name: "New Taiwan Dollar"
    },
    {
        currency_code: "TZS",
        currency_name: "Tanzanian Shilling"
    },
    {
        currency_code: "UAH",
        currency_name: "Hryvnia"
    },
    {
        currency_code: "UGX",
        currency_name: "Uganda Shilling"
    },
    {
        currency_code: "USD",
        currency_name: "US Dollar"
    },
    {
        currency_code: "UYI",
        currency_name: "Uruguay Peso en Unidades Indexadas (UI)"
    },
    {
        currency_code: "UYU",
        currency_name: "Peso Uruguayo"
    },
    {
        currency_code: "UYW",
        currency_name: "Unidad Previsional"
    },
    {
        currency_code: "UZS",
        currency_name: "Uzbekistan Sum"
    },
    {
        currency_code: "VES",
        currency_name: "Bolívar Soberano"
    },
    {
        currency_code: "VND",
        currency_name: "Dong"
    },
    {
        currency_code: "VUV",
        currency_name: "Vatu"
    },
    {
        currency_code: "WST",
        currency_name: "Tala"
    },
    {
        currency_code: "XAF",
        currency_name: "CFA Franc BEAC"
    },
    {
        currency_code: "XCD",
        currency_name: "East Caribbean Dollar"
    },
    {
        currency_code: "XOF",
        currency_name: "CFA Franc BCEAO"
    },
    {
        currency_code: "XPF",
        currency_name: "CFP Franc"
    },
    {
        currency_code: "XSU",
        currency_name: "Sucre"
    },
    {
        currency_code: "YER",
        currency_name: "Yemeni Rial"
    },
    {
        currency_code: "ZAR",
        currency_name: "Rand"
    },
    {
        currency_code: "ZMW",
        currency_name: "Zambian Kwacha"
    },
    {
        currency_code: "ZWL",
        currency_name: "Zimbabwe Dollar"
    }
]


export default currency