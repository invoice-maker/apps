import { useSelector } from 'react-redux';
import { selectAllItems } from '../model/workspace/items.slice';

export default function useGenerateCreateInvoiceJSONPayload() {

    const invoiceLabels = useSelector(state => state.invoiceLabelsConfigs)
    const invoiceMeta = useSelector(state => state.invoiceMeta)
    const invoiceItems = useSelector(state => state.invoiceItems)
    const purchasedItems = useSelector(state => selectAllItems(state))
    const exportOptions = useSelector(state => state.exportOptions)

    const payload = {
        locale: invoiceItems.locale,
        export_options: exportOptions,
        label_templates: invoiceLabels.labels,
        header: invoiceMeta,
        body: {
            show_item_discount_column: invoiceItems.enable_discount_per_item,
            currency_format: {
                currency_code: invoiceItems.currency_code,
                currency_symbol: invoiceItems.currency_symbol,
                currency_symbol_on_left: invoiceItems.currency_symbol_on_left,
                custom_currency_simbol: invoiceItems.custom_currency_simbol
            },
            charged_items: purchasedItems,
            calculation: invoiceItems.invoice_calculation
        }
    }

    return payload
}

