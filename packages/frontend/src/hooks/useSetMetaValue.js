import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { reFormatDate, setDueDateSpan, setDateValue, setStringValue, reCalculateDueDate } from '../model/workspace/meta.slice';
import dayjs from 'dayjs';

export default function useSetMetaValue(name, isDate = false, isDueDate = false) {

    const dispatch = useDispatch()
    const value = useSelector((state) => state.invoiceMeta[name])
    const invoice_date = useSelector(state => state.invoiceMeta.invoice_date.unix_date)

    // when invoice date changed, recalculate the due date
    useEffect(() => {
        dispatch(reCalculateDueDate({
            name: 'invoice_due_date'
        }))
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [invoice_date])


    const changeString = (e) => {
        dispatch(setStringValue({
            name,
            value: e.target.value
        }))

        if (name === 'date_format') {
            dispatch(reFormatDate())
        }
    }

    const changeDueDateSpan = (e) => {
        dispatch(setDueDateSpan({
            name,
            value: e.target.value
        }))
    }

    const changeDate = (date) => {
        dispatch(setDateValue({
            value: dayjs(date).valueOf(),
            name
        }))
    }

    if (isDate) {
        return [value, changeDate]
    }

    if (isDueDate) {
        return [value, changeDueDateSpan, changeDate]
    }

    return [
        value, changeString
    ]

}