export default function useInputNumeric(state, action) {

    // inputted value
    const value = action.value

    /* 
    * ================
    * STEPS WALKTROUGH
    * =================
    * 1. Make `stringValueTemp` and `floatValueTemp` as temporary container
    * 
    * 2. We assign the inputted value to those temporary container, but make check first:
    *       a. If the inputted value is `""` or `0`, 
    *          then assign `""` value to `stringValueTemp`,
    *          and assign `0` value to `floatValueTemp`.
    * 
    *          But why? 
    *          - We assign `"` to `stringValueTemp` so the input field is empty.
    *          - We assign `0` to `floatValueTemp` so the field value is 0 (zero) in float type (Empty string mean 0 value)
    *           
    *          When this kind of input usually occur?
    *          1. `""` input happen when the component first rendered
    *          2. `""` input happen when the input field is filled with valid value, 
    *             then user delete input value by pressing backspace or delete until the input field is empty
    *          3. `0` input happen when user whan to input some decimal value with leading zero, example 0.365
    * 
    *       b. If the inputted value is `-`
    *          then assing `-` value to `stringValueTemp`,
    *          and assign `0` value to `floatValueTemp`.
    * 
    *          But why? 
    *          - We assign `-` to `stringValueTemp` so the input field show `-`, 
    *            because people who type `'` maybe want to input negative value.
    *          - We assign `0` to `floatValueTemp` so the field value is 0 (zero) in float type 
    *            because when I code this at the time I think make sense to that `-` is equal to '0'
    * 
    *          When this kind of input usually occur?
    *          1. `-` input happen when user whan to input negative value like -9.326
    *             when user want to input -9.326 the first character type is  `-`
    *             if we don't allow this first `-`, it won't be possible to negative value
    * 
    *       c. If the inputted value is other than `0`, `""` and `-`
    *          then assign the inputted value to `stringValueTemp` and `floatValueTemp` as it is
    * 
    *          But why?
    *          - We assign the value into the temporary container so we can process it further.
    *            We validate it in next steps
    * 
    *          When this kind of input usually occur?
    *          1. this kind of input can happen anytime, 
    *             the input value can be `4564.464`, `asda456564asda545`, `asdasdadasdas`, `5465.51561.545646`, ANYTHING.
    *             so we need to validate it further.
    * 
    * 3. Validate that the `floatValueTemp` variable is a valid number format.
    *    1. We validate it using isNaN function, 
    *       if the `floatValueTemp` is not a valid number it doesn't pass the validation, so we do nothing with it.
    *    2. If the variable passing validation, then assign the value to the state.
    * 
    *    For reference
    *    - isNaN('123') -> false. (It'ss valid Number)
    *    - isNaN(123) -> false. (It's valid Number)
    *    - isNaN('123asd') -> true. (It's not valid Number)
    *    - isNaN('0.0314E+2') -> false. (It's valid Number)
    * 
    * 4. Validate that the `stringValueTemp` variable is a valid number, but `""` and "-" is permitted (reason: step 2a and 2b)
    *    1. We validate it using isNaN function, 
    *       if the `stringValueTemp` value is not a valid number it doesn't pass the validation (except `""` and `-`), so we do nothing with it.
    *    2. If the variable passing validation, then assign the value to the `state.stingValue` with the following rule:
    *       a. if `stringValueTemp` value is `""`, `-`, or last character contain `.` we assign those value to state as it is
    *          if you wonder why:
    *          -----------------------------------------------------
    *          stringValue | float value | input field presentation
    *          -----------------------------------------------------
    *               ""     |    0        | input field with empty string as value
    *               -      |    0        | input field with `-` string as value (just in case user want to input negative value, reason 2b)
    *              123.    |    123      | input field with `123.` as value (just in case user want to input decimal value)
    * 
    *       b. otherwise, we convert the value of `state.floatValue` to string and assign it to `state.stingValue`.
    *          If you wonder why: So the string value (presentation) is always in sync with the float value.
    * 
    * 
    * 5. Return the state as a new copy so the component rerendering
    * 
    * 
    *  -----------------------------------------------------
    *  Q: this seems complicated, why don't you just use regex?
    *  A: I have no knowledge of regex :(
    *  -----------------------------------------------------
    *  
    */

    // STEP 1
    // Make `stringValueTemp` and `floatValueTemp` as temporary container
    let stringValueTemp
    let floatValueTemp

    // STEP 2
    // We assign the inputted value to those temporary container, but make check first
    if (value === "" || value === 0) {
        stringValueTemp = ""
        floatValueTemp = 0
    } else if (value === '-') {
        stringValueTemp = "-"
        floatValueTemp = 0
    } else {
        stringValueTemp = value
        floatValueTemp = value
    }

    // STEP 3
    // Validate that the `floatValueTemp` variable is a valid number format.
    // If passing the validation, we assign the value to state.
    // Otherwise ignore the variable (do nothing with it)
    if (!isNaN(floatValueTemp)) {
        state.floatValue = parseFloat(floatValueTemp)
    }

    // STEP 4
    // Validate that the `stringValueTemp` variable is a valid number, but `""` and "-" is permitted (reason: step 2a and 2b)
    if (
        (!isNaN(stringValueTemp))
        ||
        (stringValueTemp === '' || stringValueTemp === '-')
    ) {

        state.stringValue = state.floatValue + ''

        if (
            (stringValueTemp === '' || stringValueTemp === '-')
            ||
            (stringValueTemp.slice(-1) === '.')
        ) {
            state.stringValue = stringValueTemp
        }

    }

    // STEP 5
    // Return the state as a new copy so the component rerendering
    return { ...state }
}