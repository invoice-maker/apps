import React from 'react';
import InvoiceWorkspace from './features/InvoiceWorkspace/InvoiceWorkspace';
import InvoiceSettings from './features/InvoiceSettings/InvoiceSettings';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Download from './page/Donwload';

function App() {

  const home = (
    <>
      <InvoiceWorkspace />
      <InvoiceSettings />
    </>
  )

  return (
    <Router>
      <div>

        <div className="bg-white p-1">
          <div className="flex flex-row justify-between items-center">
            <div className="text p-2 text-gray-700 text-transparent font-semibold">
              <Link to="/" >
                <h1>VIVA Invoice</h1>
              </Link>
            </div>
            <nav className="flex flex-row text-white space-x-6">
              <div>About</div>
              <div>Home</div>
            </nav>
          </div>
        </div>

        <div className="flex flex-col lg:flex-row justify-between p-4">

          {/* <Router> */}

          <Switch>
            <Route path="/download">
              <Download />
            </Route>
            <Route path="/">
              {home}
            </Route>
          </Switch>

          {/* </Router> */}
        </div>
      </div>
    </Router>
  );
}

export default App;
