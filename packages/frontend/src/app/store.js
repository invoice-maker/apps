import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import labelsReducer from '../model/workspace/labels.slice';
import metaReducer from '../model/workspace/meta.slice';
import itemsReducer from '../model/workspace/items.slice';
import exportOptionsReducer from '../model/workspace/export-options.slice';
import loadingReducer from '../model/loading.slice';

export default configureStore({
  reducer: {
    counter: counterReducer,
    invoiceLabelsConfigs: labelsReducer,
    invoiceMeta: metaReducer,
    invoiceItems: itemsReducer,
    exportOptions: exportOptionsReducer,
    loadingState: loadingReducer
  },
});