import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SettingDisclosure from './Component/SettingDisclosure';
import currency from '../../data/currency';
import { setCurrencyCode, setCurrencySymbolOnLeft, setCustomCurrencySymbol } from '../../model/workspace/items.slice';
import Input from '../../components/global/Input';
import Info from './Component/Info';
import Alert from './Component/Alert';

export default function Currency() {

    const dispatch = useDispatch()

    const itemsState = useSelector(state => state.invoiceItems)

    const options = currency.map((cur) => {
        return (
            <option key={cur.currency_code} value={cur.currency_code}>{cur.currency_code} - {cur.currency_name}</option>
        )
    })


    const info = (
        <>
            <div>
                You can change the number format (e.g. 1,123 to 1.123 or 1 123) on 'Local specific formatting' section.
            </div>
        </>
    )

    const content = (
        <>
            <div className="my-4 flex flex-col space-y-4">
                {
                    itemsState.currency_code === 'THB' ? (<Alert>If the Bath symbol is not shown, please try access this website using latest version of Chrome</Alert>): null
                }

                <select
                    onChange={(e) => dispatch(setCurrencyCode({ value: e.target.value }))}
                    value={itemsState.currency_code}
                    className="p-2 bg-white border w-full hover:ring-4 focus:outline-none focus:ring-2"
                >

                    {options}

                </select>

                <label
                    htmlFor="currencySymbolOnLeft"
                    className="flex flex-row space-x-2 items-center cursor-pointer hover:ring-4 focus:outline-none focus:ring-2">

                    <input
                        value="true"
                        className="cursor-pointer"
                        type="checkbox"
                        id="currencySymbolOnLeft"
                        checked={itemsState.currency_symbol_on_left ? true : false}
                        onChange={(e) => dispatch(setCurrencySymbolOnLeft({ value: !itemsState.currency_symbol_on_left }))}
                    />

                    <div>
                        Put currency symbol on left
                    </div>
                </label>

                <hr className="border border-yellow-300" />
                <div className="flex flex-col space-y-2">
                    <Info>
                        If currency symbol of your choice is not available, you can write your currency symbol here.
                    </Info>
                    <div className="text-sm">
                        Custom currency symbol
                    </div>
                    <Input onChange={(e) => dispatch(setCustomCurrencySymbol({ value: e.target.value }))} />
                </div>
            </div>
        </>
    )

    return (

        <SettingDisclosure
            info={info}
            title='Currency'
            content={content}
        />
    )
}