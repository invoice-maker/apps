import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SettingDisclosure from './Component/SettingDisclosure';
import Checkbox from '../../components/global/Checkbox';
import { setEnableDiscountPerItem, setTaxableShipping } from '../../model/workspace/items.slice';

export default function Options() {

    const itemDiscount = useSelector(state => state.invoiceItems.enable_discount_per_item)
    const taxableShipping = useSelector(state => state.invoiceItems.invoice_calculation.shipping_cost.taxable)

    const dispatch = useDispatch()

    const content = (

        <div className="my-4 space-y-4">

            <Checkbox
                id='enableDiscountPerItem'
                label='Enable discount per item'
                checked={itemDiscount ? 'checked' : ''}
                onChange={() => dispatch(setEnableDiscountPerItem({ value: !itemDiscount }))}
            />

            <Checkbox
                id='setShippingTaxable'
                label='Set shipping to taxable'
                checked={taxableShipping ? 'checked' : ''}
                onChange={() => dispatch(setTaxableShipping({ value: !taxableShipping }))}
            />

        </div>

    )

    return (
        <SettingDisclosure
            defaultOpen={true}
            title='Invoice options'
            content={content}
        />
    )
}