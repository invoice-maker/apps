import React, { useState } from 'react';
import { Disclosure, DisclosureButton, DisclosurePanel, } from "@reach/disclosure";
import { GoChevronDown, GoChevronUp } from 'react-icons/go';

export default function SettingDisclosure(props) {

    let {info, title, content, defaultOpen} = props

    const [isOpen, setOpen] = useState(defaultOpen || false);

    if (info) {
        info = (
            <div className="p-2 bg-yellow-200 my-1 rounded border border-yellow-300 text-xs space-y-2">
                {props.info}
            </div>
        )
    }

    return (

        <Disclosure open={isOpen} onChange={() => setOpen(!isOpen)}>

            <div>
                <DisclosureButton className="w-full font-semibold text-gray-600 text-lg hover:ring-4 focus:outline-none focus:ring-2 ring-yellow-400">
                    <div className="flex flex-row justify-between items-center">
                        <h2>
                            {title}
                        </h2>
                        <div className="text-3xl">

                            {isOpen ? <GoChevronUp /> : <GoChevronDown />}

                        </div>
                    </div>
                </DisclosureButton>

                <DisclosurePanel className="focus:outline-none text-gray-800">
                    {info}

                    {content}
                </DisclosurePanel>
            </div>
        </Disclosure>


    )
}