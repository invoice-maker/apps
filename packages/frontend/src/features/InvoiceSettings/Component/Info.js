import React from 'react'

export default function Info(props) {
    return (
        <div className="p-2 bg-yellow-200 my-1 rounded border border-yellow-300 text-xs space-y-2">
            {props.children}
        </div>
    )
}