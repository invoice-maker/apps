import React from 'react'

export default function Alert(props) {
    return (
        <div className="p-2 bg-red-200 my-1 rounded border border-red-300 text-xs space-y-2">
            {props.children}
        </div>
    )
}