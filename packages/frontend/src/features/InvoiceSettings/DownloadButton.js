import React from 'react';
import Button from '../../components/Button';
import { useHistory } from "react-router-dom";
import useGenerateCreateInvoiceJSONPayload from '../../hooks/useGenerateCreateInvoiceJSONPayload';
import {generateInvoicePDF} from '../../model/loading.slice';
import { useDispatch } from 'react-redux';

export default function DownloadButton() {

    const dispatch = useDispatch()

    const history = useHistory()

    const payload = useGenerateCreateInvoiceJSONPayload()

    // useEffect(() => {
    //     console.log(payload)
    // }, [payload])

    function handleDownload() {
        dispatch(generateInvoicePDF(payload))
        history.push('/download')
    }

    return (
        <div className="flex">

            <Button onClick={handleDownload} width="full" fontSize="xl" fontColor="white" color="blue" size="large" text="Download Invoice as PDF" customClass="w-full bg-pink-600 p-4 text-center text-white text-lg font-semibold">
            </Button>

        </div>
    )
}