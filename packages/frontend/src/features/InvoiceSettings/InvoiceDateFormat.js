import React from 'react';
import Select from '../../components/Selects';
import useSetMetaValue from '../../hooks/useSetMetaValue';
import SettingDisclosure from './Component/SettingDisclosure';

export default function InvoiceDateFormat() {

    const [value, setValue] = useSetMetaValue('date_format')

    const options = [

        {
            type: 'group',
            label: 'DAY-MONTH-YEAR',
            items: [
                { label: 'DD-MMM-YYYY (21-MAR-2020)', value: 'DD-MMM-YYYY' },
                { label: 'DD/MMM/YYYY (21/MAR/2020)', value: 'DD/MMM/YYYY' },
                { label: 'DD MMM YYYY (21 MAR 2020)', value: 'DD MMM YYYY' },

                { label: 'DD-MM-YYYY (21-03-2020)', value: 'DD-MM-YYYY' },
                { label: 'DD/MM/YYYY (21/03/2020)', value: 'DD/MM/YYYY' },
                { label: 'DD MM YYYY (21 03 2020)', value: 'DD MM YYYY' },

                { label: 'DD-MMMM-YYYY (21-March-2020)', value: 'DD-MMMM-YYYY' },
                { label: 'DD/MMMM/YYYY (21/March/2020)', value: 'DD/MMMM/YYYY' },
                { label: 'DD MMMM YYYY (21 March 2020)', value: 'DD MMMM YYYY' },
            ]
        },

        {
            type: 'group',
            label: 'MONTH-DAY-YEAR',
            items: [
                { label: 'MMM-DD-YYYY (MAR-21-2020)', value: 'MMM-DD-YYYY' },
                { label: 'MMM/DD/YYYY (MAR/21/2020)', value: 'MMM/DD/YYYY' },
                { label: 'MMM DD YYYY (MAR 21 2020)', value: 'MMM DD YYYY' },

                { label: 'MM-DD-YYYY (03-21-2020)', value: 'MM-DD-YYYY' },
                { label: 'MM/DD/YYYY (03/21/2020)', value: 'MM/DD/YYYY' },
                { label: 'MM DD YYYY (03 21 2020)', value: 'MM DD YYYY' },

                { label: 'MMMM-DD-YYYY (March-21-2020)', value: 'MMMM-DD-YYYY' },
                { label: 'MMMM/DD/YYYY (March/21/2020)', value: 'MMMM/DD/YYYY' },
                { label: 'MMMM DD YYYY (March 21 2020)', value: 'MMMM DD YYYY' },
            ]
        },

        {
            type: 'group',
            label: 'YEAR-MONTH-DATE',
            items: [
                { label: 'YYYY-MMM-DD (2020-MAR-21)', value: 'YYYY-MMM-DD' },
                { label: 'YYYY/MMM/DD (2020/MAR/21)', value: 'YYYY/MMM/DD' },
                { label: 'YYYY MMM DD (2020 MAR 21)', value: 'YYYY MMM DD' },

                { label: 'YYYY-MM-DD (2020-03-21)', value: 'YYYY-MM-DD' },
                { label: 'YYYY/MM/DD (2020/03/21)', value: 'YYYY/MM/DD' },
                { label: 'YYYY MM DD (2020 03 21)', value: 'YYYY MM DD' },

                { label: 'YYYY-MMMM-DD (2020-March-21)', value: 'YYYY-MMMM-DD' },
                { label: 'YYYY/MMMM/DD (2020/March/21)', value: 'YYYY/MMMM/DD' },
                { label: 'YYYY MMMM DD (2020 March 21)', value: 'YYYY MMMM DD' },
            ]
        },
    ];

    const info = (
        <>
            <div>Select date format for invoice date and due date.</div>
            <div>
                To translate name of the month to your language please head on to 'Local specific formatting' section.
            </div>
        </>
    )

    const content = (
        <div className="my-4 flex flex-row">
            <Select value={value} options={options} onChange={(e) => setValue(e)}></Select>
        </div>
    )


    return (

        <SettingDisclosure
            info={info}
            title='Date format'
            content={content}
        />

    )
}