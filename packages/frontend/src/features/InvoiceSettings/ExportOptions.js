import React from 'react';
import SettingDisclosure from './Component/SettingDisclosure';
import { setPaperSize } from '../../model/workspace/export-options.slice';
import { useDispatch, useSelector } from 'react-redux';

export default function ExportOptions() {

    const exportOptions = useSelector(state => state.exportOptions)

    const dispatch = useDispatch()

    const paperSize = [
        'Letter', 'Legal', 'Tabloid', 'Ledger', 'A0', 'A1', 'A2', 'A3', 'A4', 'A5', 'A6'
    ]

    const options = paperSize.map(item => {
        return (
            <option key={item} value={item}>{item}</option>
        )
    })

    const content = (

        <div className="my-4 flex flex-col">
            <div className="text-sm mb-2">
                Select paper size
            </div>
            <select onChange={(e) => dispatch(setPaperSize({ value: e.target.value }))} value={exportOptions.paper_size} className="p-2 bg-white border w-full hover:ring-4 focus:outline-none focus:ring-2">
                {options}
            </select>
        </div>

    )

    return (
        <SettingDisclosure
            title='Export options'
            content={content}
        />
    )
}