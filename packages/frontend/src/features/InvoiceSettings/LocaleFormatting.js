import React from 'react';
import SettingDisclosure from './Component/SettingDisclosure';
import locale from '../../data/locale';
import { useDispatch, useSelector } from 'react-redux';
import { setLocale } from '../../model/workspace/items.slice';

export default function LocaleFormatting() {

    const dispatch = useDispatch()
    const usedLocale = useSelector((state) => state.invoiceItems.locale)

    const info = (
        <>
        <div>
            Choose how the numbers and month names are presented based on the language of your invoice.
        </div>

        <div>
            Month name translation will be applied on invoice generation.
        </div>
        </>
    )

    const options = locale.map(item => {
        return (
            <option key={item.key} value={item.key}>{item.name}</option>
        )
    })

    const content = (

        <div className="my-4 flex flex-col">
            <div className="text-sm mb-2">
                Select language
            </div>
            <select onChange={(e) => dispatch(setLocale({ value: e.target.value }))} value={usedLocale} className="p-2 bg-white border w-full hover:ring-4 focus:outline-none focus:ring-2">
                {options}
            </select>
        </div>

    )

    return (
        <SettingDisclosure
            info={info}
            title="Local specific formatting"
            content={content}
        />
    )
}