import React from 'react';
import Currency from './Currency';
import InvoiceDateFormat from './InvoiceDateFormat';
import Options from './Options';
import LocaleFormatting from './LocaleFormatting';
import DownloadButton from './DownloadButton';
import ExportOptions from './ExportOptions';

export default function InvoiceSettings() {

    return (
        <div className="flex flex-col-reverse  w-full lg:w-80 lg:flex-col flex-shrink-0 ml-4 bg-gray-20x0 rounded p-4 space-y-8">

            <DownloadButton />
            <Currency />
            <InvoiceDateFormat />
            <LocaleFormatting />
            <ExportOptions />
            <Options />

        </div>
    )
}
