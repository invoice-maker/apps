import React from 'react';
import InvoiceHeader from './InvoiceHeader/InvoiceHeader';
import InvoiceBody from './InvoiceBody';
import InvoiceFooter from './InvoiceFooter';

export default function InvoiceWorkspace() {

    return (
        <div className="w-full shadow-lg border">
            <div className="w-full bg-white p-4 pr-2 text-gray-900 rounded flex-grow-0">
                <InvoiceHeader/>
                <InvoiceBody/>
                <InvoiceFooter/>
            </div>
        </div>
    )
}
