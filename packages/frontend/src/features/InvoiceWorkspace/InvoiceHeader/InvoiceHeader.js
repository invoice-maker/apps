import React from 'react';
import InvoiceIdentity from './InvoiceIdentity';
import InvoiceMeta from './InvoiceMeta'

export default function InvoiceHeader() {
    return (
        <div className="flex flex-col-reverse md:flex-row justify-between items-baseline my-8">
            <InvoiceIdentity />
            <InvoiceMeta />
        </div>
    )
}