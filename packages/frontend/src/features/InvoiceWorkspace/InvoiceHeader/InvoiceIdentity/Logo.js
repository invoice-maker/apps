import React from 'react';
import LabelInput from '../../../../components/LabelInput';

export default function Logo() {
    return (
        <div className="group rounded mb-6">
            <h1 className="text-3xl mb-2">
                <LabelInput labelName="business_name_label" customClass="font-semibold"/>
            </h1>
            <div className="flex flex-row invisible group-hover:visible">
                <button className="py-1 px-2 rounded bg-gray-300 mr-1 bg-blue-500 text-white hover:ring-4">
                    Text logo
                </button>
                <button className="py-1 px-2 rounded bg-gray-200 ml-1 shadow hover:ring-4">
                    Image logo
                </button>
            </div>
        </div>
    )
}