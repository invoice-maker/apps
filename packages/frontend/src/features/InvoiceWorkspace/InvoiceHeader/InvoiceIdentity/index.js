import React from 'react';
import Logo from './Logo';
import BillTo from './BillTo';
import ShipTo from './ShipTo';
import InvoiceIssuer from './InvoiceIssuer';

export default function InvoiceIdentity() {
    return (
        <div className="flex flex-col w-full" >
            <div className="flex flex-col w-full md:w-4/5">
                <Logo/>
                <InvoiceIssuer/>
            </div>
            <div className="grid gap-4 grid-cols-1 mg:grid-cols-2">
                <BillTo/>
                <ShipTo/>
            </div>
        </div>

    )
}