import React from 'react';
import LabelInput from '../../../../components/LabelInput';
import Textarea from '../../../../components/Textarea';
import useSetMetaValue from '../../../../hooks/useSetMetaValue';

export default function ShipTo() {

    const [value, onChange] = useSetMetaValue('ship_to')

    return (
        <div className="flex flex-col mt-4">
            <LabelInput labelName="ship_to_label" customClass="font-semibold" value="Ship To" />
            <div className="my-2">
                <Textarea selected={value} onChange={onChange} />
            </div>
        </div>

    )
}