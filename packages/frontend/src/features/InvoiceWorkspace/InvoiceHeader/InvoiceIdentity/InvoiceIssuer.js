import React from 'react';
import LabelTextarea from '../../../../components/LabelTextarea';

export default function InvoiceIssuer() {
    return (
        <div className="mb-6">
            <LabelTextarea labelName="invoice_issuer_information_content" rows="4" />
        </div>
    )
}