import React from 'react';
import LabelInput from '../../../../components/LabelInput';
import Textarea from '../../../../components/Textarea';
import useSetMetaValue from '../../../../hooks/useSetMetaValue';

export default function BillTo() {

    const [value, onChange] = useSetMetaValue('bill_to')

    return (
        <div className="flex flex-col mt-4">
            <LabelInput labelName="bill_to_label" customClass="font-semibold" />
            <div className="my-2">
                <Textarea selected={value} onChange={onChange}/>
            </div>
        </div>
    )
}