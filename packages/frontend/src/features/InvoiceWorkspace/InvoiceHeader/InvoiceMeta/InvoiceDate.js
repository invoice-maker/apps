import React from 'react';
import LabelInput from '../../../../components/LabelInput';
import Datepicker from '../../../../components/Datepicker';
import useSetMetaValue from '../../../../hooks/useSetMetaValue';

export default function InvoiceDate() {

    const [invoiceDateValue, changeDate] = useSetMetaValue('invoice_date', true)
    const {string_date, unix_date} = invoiceDateValue

    return (
        <div className="flex flex-row justify-end items-baseline mb-2">
            <LabelInput
                labelName="invoice_date_label"
                customClass="text-right font-semibold"
                value="Date"
            />
            <div className="w-52 flex-shrink-0 text-right ml-2">
                 <Datepicker string_date={string_date} unix_date={unix_date} onChange={changeDate}/>
            </div>


        </div>
    )
}