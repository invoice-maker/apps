import React from 'react';
import DocumentName from './DocumentName';
import InvoiceNumber from './InvoiceNumber';
import InvoiceDate from './InvoiceDate';
import PaymentTerms from './PaymentTerms';
import DueDate from './DueDate';

export default function InvoiceMeta() {
    return (
        <div className="w-full md:w-2/5 flex-shrink-0 flex flex-col md:items-end mr-8">
            <div className="flex flex-col items-end mb-8">
                <DocumentName />
                <InvoiceNumber />
            </div>
            <div className="flex flex-col w-full items-end">
                <div className="w-full">
                    <InvoiceDate/>
                    <PaymentTerms/>
                    <DueDate/>
                </div>

            </div>

        </div>
    )
}