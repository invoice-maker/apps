import React from 'react'
import LabelInput from '../../../../components/LabelInput';
import Input from '../../../../components/Input';
import useSetMetaValue from '../../../../hooks/useSetMetaValue';

export default function InvoiceNumber() {

    const [invoiceNumberValue, changeStringValue] = useSetMetaValue('invoice_number')

    return (
        <div className="flex flex-row justify-end items-baseline">

            <LabelInput
                labelName="invoice_number_label"
                customClass="text-xl font-semibold my-4 text-right flex-grow"
            />

            <div className="w-48 ml-2 flex-shrink-0">
                <Input inputClass="text-right" selected={invoiceNumberValue} onChange={changeStringValue} />
            </div>

        </div>

    )
}
