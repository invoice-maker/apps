import React from 'react';
import LabelInput from '../../../../components/LabelInput'

export default function DocumentName() {
    return (
        < LabelInput
            labelName="document_name_label"
            customClass="font-semibold text-4xl text-right"
            value="INVOICE"
        />
    )
}