import React from 'react';
import LabelInput from '../../../../components/LabelInput';
import Input from '../../../../components/Input';
import useSetMetaValue from '../../../../hooks/useSetMetaValue';


export default function PaymentTerms() {

    const [paymentTermsValue, changeStringValue] = useSetMetaValue('payment_terms')


    return (
        <div className="flex flex-row justify-end items-baseline mb-2">
            <LabelInput
                labelName="payment_terms_label"
                customClass="text-right font-semibold"
                value="Payment Terms"
            />
            <div className="w-52 flex-shrink-0 text-right ml-2">
                <Input inputClass="text-right" selected={paymentTermsValue} onChange={changeStringValue}/>
            </div>

        </div>
    )
}