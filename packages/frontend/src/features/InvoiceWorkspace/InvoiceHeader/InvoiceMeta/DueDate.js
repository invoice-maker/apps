import React from 'react';
import LabelInput from '../../../../components/LabelInput';
import Datepicker from '../../../../components/Datepicker';
import useSetMetaValue from '../../../../hooks/useSetMetaValue';
import Select from '../../../../components/Select';


export default function InvoiceDate() {

    const [dueDateValue, changeDueDateSpan, changeDate] = useSetMetaValue('invoice_due_date', false, true)
    const { due_date_span, string_date, unix_date } = dueDateValue

    let dueDate

    if (due_date_span === 'custom') {
        dueDate = <Datepicker string_date={string_date} unix_date={unix_date} onChange={changeDate} />
    } else {
        dueDate = (
            <div className="flex items-center ml-1 p-1 border border-transparent w-full flex-grow-0 overflow-x-auto">
                <div className="whitespace-nowrap w-full" >
                    {string_date ? string_date : '‎‎ ‎'}
                </div>
            </div>
        )
    }

    return (
        <div className="flex flex-row justify-end items-baseline mb-2">
            
            <LabelInput
                labelName="invoice_due_date_label"
                customClass="text-right font-semibold"
            />

            <div className="w-52 flex-shrink-0 text-right ml-2 h-full flex flex-row">

                <Select tooltip="tes"  value={due_date_span} onChange={changeDueDateSpan}>
                    <option value="">(None)</option>
                    <option value="0d">(0 day)</option>
                    <option value="7d">(7 days)</option>
                    <option value="14d">(14 days)</option>
                    <option value="1M">(1 month)</option>
                    <option value="custom">(Custom)</option>
                </Select>

                {dueDate}

            </div>


        </div>
    )
}