import React from 'react';
import LabelInput from '../../../../components/LabelInput';
import LabelTextarea from '../../../../components/LabelTextarea';

export default function AdditionalNotes() {
    return (
        <div className="grid grid-cols-1 md:grid-cols-2 gap-2 my-4">

            <div className="flex flex-col border border-gray-300">
                <div className="bg-gray-200 p-1">
                    <LabelInput
                        labelName="notes_label"
                        value="Notes"
                    />
                </div>

                <div className="p-1 text-sm">
                    <LabelTextarea
                        labelName="notes_content"
                        rwos="2"
                    />
                </div>
            </div>


            <div className="flex flex-col border border-gray-300">
                <div className="bg-gray-200 p-1">
                    <LabelInput
                        labelName="terms_and_conditions_label"
                        value="Terms"
                    />
                </div>

                <div className="p-1 text-sm">
                    <LabelTextarea
                        labelName="terms_and_conditions_content"
                        rwos="2"
                    />
                </div>
            </div>


        </div>
    )
}