import React from 'react';
import LabelTextarea from '../../../components/LabelTextarea';

export default function Footnote() {
    return (
        <div className="my-8 text-center">
            <LabelTextarea
                labelName="footnote_content"
                customClass="text-center"
                value="This invoice is computer generated no signature required "
            />
        </div>
    )
}