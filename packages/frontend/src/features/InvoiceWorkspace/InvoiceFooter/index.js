import React from 'react';
import AdditionalNotes from './AdditionalNotes';
import Footnote from './Footnote';

export default function InvoiceFooter(){
    return (
        <div>
            <AdditionalNotes/>
            <Footnote/>
        </div>
    )
}