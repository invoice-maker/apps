import React from 'react';
import Greeting from './Greeting';
import CalculationTable from './CalculationTable';
import Closing from './Closing';

export default function InvoiceBody(){
    return (
        <div className="my-8">
            <Greeting/>
            <CalculationTable/>
            <Closing/>
        </div>
    )
}