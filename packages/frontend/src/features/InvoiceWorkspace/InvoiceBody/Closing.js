import React from 'react';
import LabelTextarea from '../../../components/LabelTextarea';

export default function Closing(){
    return (
        <div className="mr-8">
            <LabelTextarea
                labelName="closing_message_content"
            />
        </div>
    )
}