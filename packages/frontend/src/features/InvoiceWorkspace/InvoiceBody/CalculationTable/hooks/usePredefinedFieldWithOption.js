import { useEffect, useReducer } from 'react';
import LabelInput from '../../../../../components/LabelInput';
import Button from '../../../../../components/Button';
import Input from '../../../../../components/Input';
import { useSelector, useDispatch } from 'react-redux';
import { setCalculationFloataValue, setCalculationStringValue, togglePredifinedCustomField } from '../../../../../model/workspace/items.slice';
import inputNumericReducer from '../../../../../reducers/inputNumericReducer';

export default function usePredefinedFieldWithOptions(props) {

    const { labelName, fieldName, buttonLabel } = props

    const field_value = useSelector(state => state.invoiceItems.invoice_calculation[fieldName].value)

    const [number, setNumber] = useReducer(inputNumericReducer, {
        stringValue: `${field_value || ''}`,
        floatValue: field_value
    })

    useEffect(() => {
        dispatch(setCalculationFloataValue({
            name: fieldName,
            value: number.stringValue
        }))
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [number]);

    const custom_field = useSelector(state => state.invoiceItems.invoice_calculation)[fieldName]

    const dispatch = useDispatch()

    let addFieldButton
    let predifinedCustomField
    if (custom_field.enabled) {

        addFieldButton = null
        predifinedCustomField = (

            <div key={labelName} className="flex flex-row relative group justify-end p-1 bg-gray-100 border-b border-gray-300">
                <div className="w-full text-right flex flex-row">
                    <LabelInput
                        labelName={labelName}
                        customClass="font-semibold text-right mr-2"
                    />

                    <select
                        onChange={(e) => {
                            dispatch(setCalculationStringValue({
                                name: fieldName,
                                value: e.target.value
                            }))
                        }}
                        value={custom_field.type}
                        className="bg-transparent -ml-1 font-semibold hover:ring-4 rounded focus:outline-none focus:ring-2">
                        <option value='flat'>(Flat)</option>
                        <option value='percent'>(Percent)</option>
                    </select>
                </div>

                <div className="w-32 flex-shrink-0 text-right pl-2 flex flex-col">

                    <Input
                        onChange={(e) => {
                            setNumber({
                                value: e.target.value
                            })
                        }}
                        inputClass="text-right"
                        selected={number.stringValue}
                        isPercent={custom_field.type === 'percent' ? true : false}
                        isCurrency={custom_field.type === 'flat' ? true : false}
                    />
                </div>

                <div className="absolute bg-transparent lg:opacity-0 left-full ml-2 border border-transparent top-0 group-hover:opacity-100 focus-within:opacity-100 font-semibold text-red-500 mt-1">
                    <Button

                        tooltip="Remove item"
                        tooltipSize="medium"
                        color="red"
                        text="✕"
                        size="small"
                        onClick={() => {
                            setNumber({ value: "" })
                            dispatch(togglePredifinedCustomField({ fieldName }))
                        }}

                    />
                </div>


            </div>
        )
    } else {
        addFieldButton = (
            <Button
                key={labelName}
                tooltip="Add global discount to calculation, to add discount per item basis you can enable it in invoice setting"
                color="white"
                text={buttonLabel}
                onClick={() => {
                    dispatch(togglePredifinedCustomField({ fieldName }))
                }}
            />
        )
        predifinedCustomField = null
    }


    return [
        addFieldButton,
        predifinedCustomField
    ]


}