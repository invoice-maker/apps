import React, { useEffect } from 'react'
import LabelInput from '../../../../components/LabelInput';
import LabelCurrency from '../../../../components/LabelCurrency';
import { useSelector, useDispatch } from 'react-redux';
import { calculateSubTotal, selectAllItems, calculateBalanceDue } from '../../../../model/workspace/items.slice';
import usePredefinedFieldWithOptions from './hooks/usePredefinedFieldWithOption';
import usePredefinedFieldWithoutOptions from './hooks/usePredefinedFieldWithoutOption';

export default function TotalCalculation() {

    const [addInvoiceDiscountButton, invoiceDiscountComponent] = usePredefinedFieldWithOptions({
        labelName: 'global_discount_label',
        fieldName: 'global_invoice_discount',
        buttonLabel: '+ Invoice discount'
    })

    const [addInvoiceTaxButton, invoiceTaxComponent] = usePredefinedFieldWithOptions({
        labelName: 'tax_label',
        fieldName: 'tax',
        buttonLabel: '+ Invoice tax'
    })

    const [addShippingCostButton, shippingCostComponnet] = usePredefinedFieldWithoutOptions({
        labelName: 'shipping_label',
        fieldName: 'shipping_cost',
        buttonLabel: '+ Shipping'
    })

    const optionButtons = [
        addInvoiceDiscountButton,
        addInvoiceTaxButton,
        addShippingCostButton
    ]

    const itemsState = useSelector(state => selectAllItems(state))
    
    const invoice_calculation = useSelector(state => state.invoiceItems.invoice_calculation)
    
    const isShippingTaxable = useSelector(state => state.invoiceItems.invoice_calculation.shipping_cost.taxable)

    const { invoice_subtotal, balance_due } = invoice_calculation

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(calculateSubTotal())
    }, [itemsState]) // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        dispatch(calculateBalanceDue())
    }, [invoice_subtotal]) // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        dispatch(calculateBalanceDue())
    }, [invoice_calculation]) // eslint-disable-line react-hooks/exhaustive-deps


    return (
        <React.Fragment>
            <div className="flex flex-col border-t border-b border-black font-semibold mr-8">
                <div className="flex flex-row justify-end items-baseline p-1 bg-gray-100 border-b border-gray-300">
                    <div className="w-full text-right">
                        <LabelInput
                            labelName="subtotal_label"
                            customClass="font-semibold text-right"
                            value="Subtotal"
                        />
                    </div>

                    <div className="w-32 flex-shrink-0 text-right">
                        <LabelCurrency value={invoice_subtotal} currencyClass="ml-1" />
                    </div>
                </div>

                {invoiceDiscountComponent}
                {isShippingTaxable ? shippingCostComponnet : null}
                {invoiceTaxComponent}
                {isShippingTaxable ? null : shippingCostComponnet}


                <div className="flex flex-row items-baseline justify-end p-1 bg-gray-200 text-lg">
                    <LabelInput
                        labelName="balance_due_label"
                        customClass="font-semibold text-right"
                    />
                    <div className="w-32 flex-shrink-0 text-right">
                        <LabelCurrency value={balance_due} currencyClass="ml-1" />
                    </div>
                </div>


            </div>

            {
                optionButtons.some((item) => item !== null) ?
                    <div className="flex justify-end">
                        <div className="flex flex-col w-full md:flex-row flex-wrap inline-flex justify-end mr-8 p-2 border border-black border-t-0 rounded-t-none bg-gray-200 md:space-x-2 space-y-2 md:space-y-0">
                            {optionButtons}
                        </div>
                    </div>
                    : null
            }

        </React.Fragment>
    )
}