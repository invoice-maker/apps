import React from 'react';
import HeaderTable from './HeaderTable'
import PurchasedItems from './PurchasedItems/index'
// import ChargedItems from './ChargedItems';
import TotalCalculation from './TotalCalculation';

export default function CalculationTable() {
    return (
        <div className="my-4 border-black">
            <HeaderTable />
            <PurchasedItems />
            <TotalCalculation />
        </div>
    )
}