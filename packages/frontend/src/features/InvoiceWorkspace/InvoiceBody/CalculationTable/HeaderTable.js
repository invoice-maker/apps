import React from "react";
import { useSelector } from "react-redux";
import LabelInput from "../../../../components/LabelInput";

export default function HeaderTable() {
  const isItemDiscountEnabled = useSelector(
    (state) => state.invoiceItems.enable_discount_per_item
  );

  return (
    <>
      <div className="font-semibold">Column name:</div>
      <div className="flex flex-col space-y-2 md:space-y-0 md:flex-row justify-between p-1 border-t border-b border-black text-center bg-gray-100 font-semibold md:mr-7">
        <div className="w-full">
          <LabelInput
            labelName="item_description_label"
            customClass="text-center bg-gray-100 font-semibold"
            value="Item Description"
          />
        </div>

        <div className="w-full md:w-32 flex-shrink-0 ml-2">
          <LabelInput
            labelName="item_qty_label"
            customClass="text-center bg-gray-100 font-semibold"
            value="Hour/Qty"
          />
        </div>

        <div className="w-full md:w-32 flex-shrink-0 ml-2">
          <LabelInput
            labelName="item_price_label"
            customClass="text-center bg-gray-100 font-semibold"
            value="Rate/Price"
          />
        </div>

        {isItemDiscountEnabled ? (
          <div className="w-full md:w-36 flex-shrink-0 ml-2">
            <LabelInput
              labelName="item_discount_label"
              customClass="text-center bg-gray-100 font-semibold"
              value="Discount"
            />
          </div>
        ) : null}

        <div className="w-full md:w-32 flex-shrink-0 ml-2">
          <LabelInput
            labelName="item_amount_label"
            customClass="text-center bg-gray-100 font-semibold"
            value="Amount"
          />
        </div>
      </div>
    </>
  );
}
