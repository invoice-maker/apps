import React from 'react'
import { useSelector } from 'react-redux';
import { selectAllItems } from '../../../../../model/workspace/items.slice';
import Items from './Items';
import AddNewButton from './AddNewItemButton';

export default function ChargedItems() {

    const itemsState = useSelector(state => selectAllItems(state))

    const items = itemsState.map(item => {
        if (!item) return null
        return (<Items key={item.item_uid} attr={item} />)
    })


    return (
        <div className="flex flex-col">

            {items}

            <AddNewButton />

        </div>
    )
}