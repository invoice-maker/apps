import React, { useEffect } from 'react'
import Button from '../../../../../components/Button';
import { useSelector, useDispatch } from 'react-redux';
import { selectAllItems, addNewEmptyItem } from '../../../../../model/workspace/items.slice';

export default function AddNewButton() {

    const itemsState = useSelector(state => selectAllItems(state))

    const dispatch = useDispatch()

    useEffect(() => {

        if (itemsState.length === 0) return

        const lastItemUid = itemsState[itemsState.length - 1].item_uid
        const lastInputElementId = `item_name-${lastItemUid}`

        if (itemsState.length > 1) {
            document.getElementById(lastInputElementId).focus()
        }

    }, [itemsState.length]) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <div className="inline-block text-center my-2">
            <Button
                onClick={() => dispatch(addNewEmptyItem())}
                color="yellow"
                text="Add new item"
            />
        </div>
    )
}