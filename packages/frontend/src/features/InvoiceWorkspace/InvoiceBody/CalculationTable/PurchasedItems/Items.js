import React, { useEffect, useReducer } from "react";
import Input from "../../../../../components/Input";
import Textarea from "../../../../../components/Textarea";
import Button from "../../../../../components/Button";
import LabelCurrency from "../../../../../components/LabelCurrency";
import Select from "../../../../../components/Select";
import { useDispatch, useSelector } from "react-redux";
import {
  setItemStringValue,
  setItemFloatValue,
  removeItem,
} from "../../../../../model/workspace/items.slice";
import inputNumericReducer from "../../../../../reducers/inputNumericReducer";

export default function Items(props) {
  const {
    item_uid,
    item_name,
    item_description,
    item_amount,
    item_discount_type,
  } = props.attr;

  const isItemDiscountEnabled = useSelector(
    (state) => state.invoiceItems.enable_discount_per_item
  );

  const itemState = useSelector(
    (state) => state.invoiceItems.entities[item_uid]
  );

  const dispatch = useDispatch();

  const [qty, setQty] = useReducer(inputNumericReducer, {
    stringValue: `${itemState.item_qty || ""}`,
    floatValue: itemState.item_qty,
  });

  const [price, setPrice] = useReducer(inputNumericReducer, {
    stringValue: `${itemState.item_price || ""}`,
    floatValue: itemState.item_price,
  });

  const [discount, setDiscount] = useReducer(inputNumericReducer, {
    stringValue: `${itemState.item_discount || ""}`,
    floatValue: itemState.item_discount,
  });

  useEffect(() => {
    if (!qty.action) return;
    if (!qty.action.item_uid) return;

    const { name, item_uid } = qty.action;
    dispatch(
      setItemFloatValue({
        name,
        item_uid,
        value: qty.floatValue,
      })
    );
  }, [qty]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (!price.action) return;
    if (!price.action.item_uid) return;

    const { name, item_uid } = price.action;
    dispatch(
      setItemFloatValue({
        name,
        item_uid,
        value: price.floatValue,
      })
    );
  }, [price]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (!discount.action) return;
    if (!discount.action.item_uid) return;

    const { name, item_uid } = discount.action;
    dispatch(
      setItemFloatValue({
        name,
        item_uid,
        value: discount.floatValue,
      })
    );
  }, [discount]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (isItemDiscountEnabled === false) {
      setDiscount({
        value: "",
        name: "item_discount",
        item_uid,
      });
    }
  }, [isItemDiscountEnabled]); // eslint-disable-line react-hooks/exhaustive-deps

  const changeStringValue = (e, name, item_uid) => {
    dispatch(
      setItemStringValue({
        name,
        item_uid,
        value: e.target.value,
      })
    );
  };

  return (
    <div className="flex flex-col md:flex-row space-y-2 md:space-y-0 justify-between p-1 group relative pr-8 items-baseline border-b md:border-none mb-8 md:mb-0">
      <div className="flex flex-col w-full">
        <div className="font-semibold md:hidden">Item Name:</div>

        <div className="w-full">
          <Input
            id={`item_name-${item_uid}`}
            selected={item_name}
            onChange={(e) => changeStringValue(e, "item_name", item_uid)}
          />
        </div>
        <div className="my-1 text-sm">
          <div className="font-semibold text-base md:hidden">
            Item Description:
          </div>
          <Textarea
            selected={item_description}
            onChange={(e) => changeStringValue(e, "item_description", item_uid)}
          />
        </div>
      </div>

      <div className="w-full md:w-32 flex-shrink-0 md:text-center md:ml-2">
        <div className="font-semibold md:hidden">Quantity:</div>
        <Input
          inputClass="text-right"
          selected={qty.stringValue}
          onChange={(e) =>
            setQty({
              value: e.target.value,
              name: "item_qty",
              item_uid,
            })
          }
        />
      </div>

      <div className="w-full md:w-32 flex-shrink-0 md:text-center md:ml-2">
        <div className="font-semibold text-base md:hidden">Price:</div>
        <Input
          inputClass="text-right"
          isCurrency={true}
          selected={price.stringValue}
          onChange={(e) =>
            setPrice({
              value: e.target.value,
              name: "item_price",
              item_uid,
            })
          }
        />
      </div>

      {isItemDiscountEnabled ? (
        <div>
          <div className="font-semibold text-base md:hidden">Discount:</div>

          <div className="w-full md:w-36 flex-shrink-0 text-center md:ml-2 flex flex-row h-full">
            <Select
              value={item_discount_type}
              onChange={(e) =>
                changeStringValue(e, "item_discount_type", item_uid)
              }
            >
              <option value="flat">F</option>
              <option value="percent">%</option>
            </Select>

            <Input
              inputClass="text-right"
              isCurrency={item_discount_type === "flat" ? true : false}
              isPercent={item_discount_type === "percent" ? true : false}
              selected={discount.stringValue}
              onChange={(e) =>
                setDiscount({
                  value: e.target.value,
                  name: "item_discount",
                  item_uid,
                })
              }
            />
          </div>
        </div>
      ) : null}

      <div className="w-full md:w-32 flex-shrink-0 md:text-center md:ml-2">
      <div className="font-semibold text-base md:hidden">Amount:</div>

        <LabelCurrency
          value={item_amount}
          currencyClass="font-semibold"
          valueClass="font-semibold"
        />
      </div>

      <div className="absolute lg:opacity-0 right-0 top-0 group-hover:opacity-100 focus-within:opacity-100 font-semibold text-red-500 mt-1">
        <Button
          tooltip="Remove item"
          tooltipSize="medium"
          color="red"
          text="✕"
          size="small"
          onClick={(e) => dispatch(removeItem({ item_uid }))}
        />
      </div>
    </div>
  );
}
