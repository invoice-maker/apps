import React, { useEffect, useReducer } from 'react'
import Input from '../../../../components/Input';
import Textarea from '../../../../components/Textarea';
import Button from '../../../../components/Button';
import LabelCurrency from '../../../../components/LabelCurrency';
import Select from '../../../../components/Select';
import { useSelector, useDispatch } from 'react-redux';
import { selectAllItems, setItemStringValue, setItemFloatValue, removeItem, addNewEmptyItem, itemsAdapter } from '../../../../model/workspace/items.slice';
import inputNumericReducer from '../../../../reducers/inputNumericReducer';

export default function ChargedItems() {

    const itemsState = useSelector(state => selectAllItems(state))

    const [qty, setQty] = useReducer(inputNumericReducer, {
        stringValue: "",
        floatValue: 0
    })

    const [price, setPrice] = useReducer(inputNumericReducer, {
        stringValue: "",
        floatValue: 0
    })

    const [discount, setDiscount] = useReducer(inputNumericReducer, {
        stringValue: "",
        floatValue: 0
    })


    const dispatch = useDispatch()

    // focus the key to last input when the number of key changed
    useEffect(() => {

        const lastItemUid = itemsState[itemsState.length - 1].item_uid
        const lastInputElementId = `item_name-${lastItemUid}`

        if (itemsState.length > 1) {
            document.getElementById(lastInputElementId).focus()
        }

    }, [itemsState.length])

    const changeStringValue = (e, name, item_uid) => {
        dispatch(setItemStringValue({
            name,
            item_uid,
            value: e.target.value
        }))
    }

    const changeFloatValue = (e, name, item_uid) => {

        const rawValue = e.target.value || 0

        const value = parseFloat(rawValue)

        if (isNaN(value) || isNaN(rawValue)) {
            return
        }

        dispatch(setItemFloatValue({
            name,
            item_uid,
            value: rawValue
        }))

    }

    const items = itemsState.map(item => {

        if (!item) {
            return
        }

        const { item_uid, item_name, item_description, input_container, item_discount, item_amount, item_discount_type } = item
        return (
            <div key={item_uid} className="flex flex-row justify-between p-1 group relative pr-8 items-baseline">
                <div className="flex flex-col w-full">
                    <div className="w-full">
                        <Input id={`item_name-${item_uid}`} selected={item_name} onChange={(e) => changeStringValue(e, "item_name", item_uid)} />
                    </div>
                    <div className="my-1 text-sm">
                        <Textarea selected={item_description} onChange={(e) => changeStringValue(e, "item_description", item_uid)} />
                    </div>
                </div>

                <div className="w-32 flex-shrink-0 text-center ml-2">
                    <Input inputClass="text-right" selected={input_container.item_qty} onChange={(e) => changeFloatValue(e, "item_qty", item_uid)} />
                </div>

                <div className="w-32 flex-shrink-0 text-center ml-2">
                    <Input inputClass="text-right" selected={input_container.item_price} isCurrency={true} onChange={(e) => changeFloatValue(e, "item_price", item_uid)} />
                </div>


                <div className="w-36 flex-shrink-0 text-center ml-2 flex flex-row h-full">

                    <Select value={item_discount_type} onChange={(e) => changeStringValue(e, "item_discount_type", item_uid)}>
                        <option value="flat" >F</option>
                        <option value="percent">%</option>
                    </Select>

                    <Input
                        inputClass="text-right"
                        selected={input_container.item_discount}
                        isCurrency={item_discount_type === 'flat' ? true : false}
                        isPercent={item_discount_type === 'percent' ? true : false}
                        onChange={(e) => changeFloatValue(e, "item_discount", item_uid)}
                    />

                </div>


                <div className="w-32 flex-shrink-0 text-center ml-2">
                    <LabelCurrency value={item_amount} currencyClass="font-semibold" valueClass="font-semibold" />
                </div>

                <div className="absolute opacity-0 right-0 top-0 group-hover:opacity-100 focus-within:opacity-100 font-semibold text-red-500 mt-1">
                    <Button
                        tooltip="Remove item"
                        tooltipSize="medium"
                        color="red"
                        text="✕"
                        size="small"
                        onClick={(e) => dispatch(removeItem({ item_uid }))}

                    />
                </div>

            </div>
        )
    })


    return (
        <div className="flex flex-col">


            {items}

            <div className="inline-block text-center my-2">
                <Button onClick={() => dispatch(addNewEmptyItem())} color="yellow" text="Add new item" />
            </div>

        </div>
    )
}