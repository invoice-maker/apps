import React from 'react';
import LabelTextarea from '../../../components/LabelTextarea';

export default function Greeting() {
    return (
        <div className="mr-8">
            <LabelTextarea
                labelName="greeting_message_content"
                value= "Dear Customer, \n\n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu turpis pharetra, ullamcorper urna sit amet, blandit risus. Ut dapibus euismod ligula, at hendrerit nulla gravida vel. Maecenas scelerisque nisi a mi pulvinar gravida in non sem. In sed velit odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultricies sapien felis,"
            />
        </div>
    )
}