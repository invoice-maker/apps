const firebase = require("firebase-admin");
const serviceAccount = require("../data/serviceAccountFirebase.json");

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    storageBucket: "invoice-maker-a7a36-tmp"
});

const bucket = firebase.storage().bucket();

module.exports = {
    bucket
}
