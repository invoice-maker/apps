const template = require('../../templates/invoice/mobile');
const puppeteer = require('puppeteer');
const bucket = require('../../provider/firebase').bucket;
const { nanoid } = require('nanoid')

async function generateInvoice(req, res) {

    const payload = req.body

    const generatedHtml = template(payload)

    const browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    });

    const page = await browser.newPage();

    await page.setContent(generatedHtml);

    const fileData = await page.pdf({
        format: payload.export_options.paper_size,
        scale: 0.8,
        printBackground: true,
        margin: {
            top: '0.4in',
            right: '0.4in',
            left: '0.4in',
            bottom: '0.4in'
        }
    });

    const fileName = `INV-${nanoid(10)}.pdf`

    await browser.close();

    const tmpFile = bucket.file(fileName)

    try {
        await tmpFile.save(fileData, {
            gzip: true,
            resumable: false,
            private: true
        })

    } catch (error) {
        return res.status(500).end()
    }

    const signedUrl = await bucket.file(fileName).getSignedUrl({
        version: 'v4',
        action: 'read',
        expires: Date.now() + 1 * 60 * 1000, // 1 minutes
    });

    return res.json({
        status: 'success',
        data: {
            download_url: signedUrl[0]
        }
    })

}

module.exports = generateInvoice