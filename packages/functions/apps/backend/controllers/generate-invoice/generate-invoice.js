const template = require('../../templates/invoice/default');
const puppeteer = require('puppeteer');

async function generateInvoice(req, res) {

    const payload = req.body

    const generatedHtml = template(payload)

    const browser = await puppeteer.launch({ 
        args: [ '--no-sandbox', '--disable-setuid-sandbox']
     });

    const page = await browser.newPage();

    await page.setContent(generatedHtml);

    const fileData = await page.pdf({
        format: payload.export_options.paper_size,
        scale: 0.8,
        printBackground: true,
        margin: {
            top: '0.4in',
            right: '0.4in',
            left: '0.4in',
            bottom: '0.4in'
        }
    });
    
    const fileName = `INV${payload.header.invoice_number}.pdf`

    const fileType = 'application/pdf'
    
    await browser.close();
    
    res.writeHead(200, {
        'Content-Disposition': `attachment; filename="${fileName}"`,
        'Content-Type': fileType,
    })
    
    const download = Buffer.from(fileData, 'base64')
    
    return res.end(download)
}

module.exports = generateInvoice