const express = require('express');
const router = express.Router();
const generateInvoiceController = require('../controllers/generate-invoice/generate-invoice');
const convertNlToBr = require('../middlewares/convertNlToBr');
const convertDateToLocale = require('../middlewares/convertDateToLocale');
const convertNumberToCurrencyString = require('../middlewares/convertNumberToCurrencyString');
const convertPercentValueCalculationToCurrencyAmount = require('../middlewares/convertPercentValueCalculationToCurrencyAmount');

const _m_convertNlToBr = require('../mobileMiddlewares/convertNlToBr');
const _m_convertDateToLocale = require('../mobileMiddlewares/convertDateToLocale');
const _m_convertNumberToCurrencyString = require('../mobileMiddlewares/convertNumberToCurrencyString');
const _m_convertPercentValueCalculationToCurrencyAmount = require('../mobileMiddlewares/convertPercentValueCalculationToCurrencyAmount');
const _m_generateInvoiceController = require('../controllers/generate-invoice/generate-invoice-mobile');


router.post('/',
    convertNlToBr,
    convertDateToLocale,
    convertPercentValueCalculationToCurrencyAmount,
    convertNumberToCurrencyString,
    generateInvoiceController
);

router.post('/generate-invoice',
    _m_convertNlToBr,
    _m_convertDateToLocale,
    _m_convertPercentValueCalculationToCurrencyAmount,
    _m_convertNumberToCurrencyString,
    _m_generateInvoiceController
)


module.exports = router;