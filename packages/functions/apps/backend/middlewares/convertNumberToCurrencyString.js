function convertNumberToCurrencyString(req, res, next) {

    const payload = req.body

    const locale = payload.locale

    const numberToCurrency = (locale, number) => {
        return new Intl.NumberFormat(locale, { style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(number)
    }

    payload.body.charged_items.forEach((item, index) => {
        payload.body.charged_items[index].item_price = numberToCurrency(locale, item.item_price)
        payload.body.charged_items[index].item_amount = numberToCurrency(locale, item.item_amount)

        if (item.item_discount_type === 'flat') {
            payload.body.charged_items[index].item_discount = numberToCurrency(locale, item.item_discount)
        }
    });

    payload.body.calculation.invoice_subtotal = numberToCurrency(locale, payload.body.calculation.invoice_subtotal)
    payload.body.calculation.shipping_cost.value = numberToCurrency(locale, payload.body.calculation.shipping_cost.value)
    payload.body.calculation.balance_due = numberToCurrency(locale, payload.body.calculation.balance_due)

    payload.body.calculation.global_invoice_discount.computed_value = numberToCurrency(locale, payload.body.calculation.global_invoice_discount.computed_value)
    payload.body.calculation.tax.computed_value = numberToCurrency(locale, payload.body.calculation.tax.computed_value)
    
    if (payload.body.calculation.global_invoice_discount.type === 'flat') {
        payload.body.calculation.global_invoice_discount.value = numberToCurrency(locale, payload.body.calculation.global_invoice_discount.value)
    }

    if (payload.body.calculation.tax.type === 'flat') {
        payload.body.calculation.tax.value = numberToCurrency(locale, payload.body.calculation.tax.value)
    }

    return next()

}

module.exports = convertNumberToCurrencyString