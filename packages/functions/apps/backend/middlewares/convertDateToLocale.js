const dayjs = require('dayjs')

function convertDateToLocale(req, res, next) {

    const payload = req.body

    require(`dayjs/locale/${payload.locale}`)

    payload.header.invoice_date.string_date = dayjs(payload.header.invoice_date.unix_date).locale(payload.locale).format(payload.header.date_format)

    if (payload.header.invoice_due_date.unix_date) {
        payload.header.invoice_due_date.string_date = dayjs(payload.header.invoice_due_date.unix_date).locale(payload.locale).format(payload.header.date_format)
    }

    return next()
}

module.exports = convertDateToLocale