function convertPercentValueCalculationToCurrencyAmount(req, res, next) {

    const payload = req.body

    // calculate discount in currency
    let discount
    let total_after_discount
    if (payload.body.calculation.global_invoice_discount.type === 'percent') {

        discount = payload.body.calculation.invoice_subtotal * (payload.body.calculation.global_invoice_discount.value / 100);

        // payload.label_templates.global_discount_label = `${payload.label_templates.global_discount_label} (${payload.body.calculation.global_invoice_discount.value}%)`
        payload.body.calculation.global_invoice_discount.computed_value = discount
        total_after_discount = payload.body.calculation.invoice_subtotal - payload.body.calculation.global_invoice_discount.computed_value
    }else{
        total_after_discount = payload.body.calculation.invoice_subtotal - payload.body.calculation.global_invoice_discount.value 
    }
    
    
    // convert tax percent to currency
    let total_after_taxable_shipping
    if(payload.body.calculation.shipping_cost.taxable){

        // convert tax percent to currency if shipping is taxable
        total_after_taxable_shipping = total_after_discount + payload.body.calculation.shipping_cost.value

        if(payload.body.calculation.tax.type === 'percent'){
            payload.body.calculation.tax.computed_value = total_after_taxable_shipping * (payload.body.calculation.tax.value/100)
        }

        return next()

    }else{
        
        // convert tax percent to currency if shipping is not taxable
        if(payload.body.calculation.tax.type === 'percent'){
            payload.body.calculation.tax.computed_value = total_after_discount * (payload.body.calculation.tax.value/100)
        }

        return next()

    }


}

module.exports = convertPercentValueCalculationToCurrencyAmount