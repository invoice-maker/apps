const nl2br = require('nl2br')

function convertNlToBr(req, res, next) {

    const payload = req.body

    payload.label_templates.invoice_issuer_information_content = nl2br(payload.label_templates.invoice_issuer_information_content || "")

    payload.label_templates.greeting_message_content = nl2br(payload.label_templates.greeting_message_content || "")

    payload.label_templates.closing_message_content = nl2br(payload.label_templates.closing_message_content || "")

    payload.label_templates.footnote_content = nl2br(payload.label_templates.footnote_content || "")

    payload.label_templates.terms_and_conditions_content = nl2br(payload.label_templates.terms_and_conditions_content || "")

    payload.label_templates.notes_content = nl2br(payload.label_templates.notes_content || "")

    payload.header.bill_to = nl2br(payload.header.bill_to || "")

    payload.header.ship_to = nl2br(payload.header.ship_to || "")

    payload.body.charged_items.forEach((value, index) => {
        payload.body.charged_items[index].item_description = nl2br(value.item_description || "")
    })

    return next()
}

module.exports = convertNlToBr