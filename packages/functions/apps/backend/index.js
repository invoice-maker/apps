const functions = require('firebase-functions');
const express = require('express');
const app = express();
const routes = require('./routes');
const cors = require('cors')


// global middlewares
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// registering router
app.use('/v1', routes);

exports.app = functions.https.onRequest(app);


