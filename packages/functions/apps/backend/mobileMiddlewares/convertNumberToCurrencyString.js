function convertNumberToCurrencyString(req, res, next) {

    const payload = req.body

    const locale = payload.document_body.currency_format.number_format

    const numberToCurrency = (locale, number) => {
        return new Intl.NumberFormat(locale, { style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(number)
    }

    payload.document_body.charged_items.forEach((item, index) => {
        payload.document_body.charged_items[index].item_price = numberToCurrency(locale, item.item_price)
        payload.document_body.charged_items[index].item_amount = numberToCurrency(locale, item.item_amount)

        if (item.item_discount_type === 'flat') {
            payload.document_body.charged_items[index].item_discount = numberToCurrency(locale, item.item_discount)
        }
    });

    payload.document_body.calculation_components.invoice_subtotal = numberToCurrency(locale, payload.document_body.calculation_components.invoice_subtotal)
    payload.document_body.calculation_components.shipping_cost.value = numberToCurrency(locale, payload.document_body.calculation_components.shipping_cost.value)
    payload.document_body.calculation_components.balance_due = numberToCurrency(locale, payload.document_body.calculation_components.balance_due)

    payload.document_body.calculation_components.global_invoice_discount.computed_value = numberToCurrency(locale, payload.document_body.calculation_components.global_invoice_discount.computed_value)
    payload.document_body.calculation_components.tax.computed_value = numberToCurrency(locale, payload.document_body.calculation_components.tax.computed_value)
    
    if (payload.document_body.calculation_components.global_invoice_discount.type === 'flat') {
        payload.document_body.calculation_components.global_invoice_discount.value = numberToCurrency(locale, payload.document_body.calculation_components.global_invoice_discount.value)
    }

    if (payload.document_body.calculation_components.tax.type === 'flat') {
        payload.document_body.calculation_components.tax.value = numberToCurrency(locale, payload.document_body.calculation_components.tax.value)
    }

    return next()

}

module.exports = convertNumberToCurrencyString