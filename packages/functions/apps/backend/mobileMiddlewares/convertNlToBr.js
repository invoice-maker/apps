const nl2br = require('nl2br')

function convertNlToBr(req, res, next) {

    const payload = req.body

    payload.document_labels.invoice_issuer_information_content = nl2br(payload.document_labels.invoice_issuer_information_content || "")

    payload.document_labels.greeting_message_content = nl2br(payload.document_labels.greeting_message_content || "")

    payload.document_labels.closing_message_content = nl2br(payload.document_labels.closing_message_content || "")

    payload.document_labels.footnote_content = nl2br(payload.document_labels.footnote_content || "")

    payload.document_labels.terms_and_conditions_content = nl2br(payload.document_labels.terms_and_conditions_content || "")

    payload.document_labels.notes_content = nl2br(payload.document_labels.notes_content || "")

    payload.document_references.bill_to = nl2br(payload.document_references.bill_to || "")

    payload.document_references.ship_to = nl2br(payload.document_references.ship_to || "")

    payload.document_body.charged_items.forEach((value, index) => {
        payload.document_body.charged_items[index].item_description = nl2br(value.item_description || "")
    })

    return next()
}

module.exports = convertNlToBr