const dayjs = require('dayjs')

function convertDateToLocale(req, res, next) {

    const payload = req.body

    require(`dayjs/locale/${payload.document_references.date_local}`)

    payload.document_references.invoice_date.string_date = dayjs(payload.document_references.invoice_date.unix_date).locale(payload.document_references.date_local).format(payload.document_references.date_format)

    if (payload.document_references.invoice_due_date.unix_date) {
        payload.document_references.invoice_due_date.string_date = dayjs(payload.document_references.invoice_due_date.unix_date).locale(payload.document_references.date_local).format(payload.document_references.date_format)
    }

    return next()
}

module.exports = convertDateToLocale